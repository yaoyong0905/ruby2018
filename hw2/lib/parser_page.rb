class Seller
  	attr_accessor :seller_name, :seller_id, :feedback ,:ratings
	def parser_file(file_name)
		a = File.read(file_name)
		seller = Array.new()

		if a.include? "ssl-images-amazon.com" 
			seller[0] ="Amazon"
			seller[1]= nil
			seller[2]=  nil
			seller[3]=  nil

		elsif a.include? "olpJustLaunched" 
			# seller=AYXDDSK9W0RO9">Kathysshop</a></span></h3>
			matchres = a.match /seller=(\w+)\">(.*?)<\/a>/ 
			seller[0] = matchres[2]   
			seller[1] = matchres[1]  
			seller[2]=  nil
			seller[3]=  nil 

		else 
			# seller=A19A29L7PVBL05">D M Books Store</a> </span>
			matchres = a.match /seller=(\w+)\">(.*?)<\/a>/
 
			seller[0] = matchres[2]  
			seller[1] = matchres[1] 

			matchres1 = a.match /(\d+)% positive/
			seller[2] = matchres1[1]

			matchres2 = a.match /(\d+) total ratings/
			seller[3] = matchres2[1] 
 
		end

		return seller
	end
end