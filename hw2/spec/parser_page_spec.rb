require "parser_page.rb"


RSpec.describe "parser_page.rb" do
  it "Seller.parser_file(seller-amazon.html)" do
  	B = Seller.new
  	a = B.parser_file("seller-amazon.html")
  	expect(a[0]).to eq('Amazon') 
  	expect(a[1]).to eq(nil) 
  	expect(a[2]).to eq(nil) 
  	expect(a[3]).to eq(nil) 
  end


  it "Seller.parser_file(seller-just-launched.html)" do
  	B = Seller.new
  	a = B.parser_file("seller-just-launched.html")
  	expect(a[0]).to eq('Kathysshop') 
  	expect(a[1]).to eq('AYXDDSK9W0RO9') 
  	expect(a[2]).to eq(nil) 
  	expect(a[3]).to eq(nil)  
  end

  it "Seller.parser_file(seller-normal.html)" do
  	B = Seller.new
  	a = B.parser_file("seller-normal.html")
  	expect(a[0]).to eq("D M Books Store") 
  	expect(a[1]).to eq("A19A29L7PVBL05") 
  	expect(a[2]).to eq("92") 
  	expect(a[3]).to eq("409") 
  end
  


end
 