class Category < ActiveRecord::Base
  belongs_to :user,
             counter_cache: true
  has_many :topics,
           dependent: :destroy
  has_many :posts, through: :topics


  def self.for_select
    Category.all.map do |category|
      [category.name, category.id]
    end
  end
end