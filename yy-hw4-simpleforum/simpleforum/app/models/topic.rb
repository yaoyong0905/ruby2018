class Topic < ActiveRecord::Base
  belongs_to :category,
             counter_cache: true,
             touch: true

  belongs_to :user,
             counter_cache: true

  has_many :posts,
           dependent: :destroy
end