class Post < ActiveRecord::Base
  belongs_to :topic,
             counter_cache: true,
             touch: true
  belongs_to :user,
             counter_cache: true
  has_one :category, through: :topics
end