class PostsController < ApplicationController
  before_action :set_topic
  before_action :set_post, only: [:edit, :update, :destroy]
  respond_to :js

  def new
    @post = Post.new
  end

  def create
    @post = Post.new(post_params)
    @post.user = current_user
    @post.topic = @topic

    unless @post.save
      render 'topics/show', alert: 'Post was not created.'
    end
  end

  def edit
  end

  def update
    unless @post.update(post_params)
      render :edit, alert: 'Post was not updated.'
    end
  end

  def destroy
    @post.destroy
  end

  private
    def set_topic
      @topic = Topic.find_by_id(params[:topic_id])
    end

    def set_post
      @post = Post.find_by_id(params[:id])
    end

    def post_params
      params.require(:post).permit(:content, :priority)
    end
end