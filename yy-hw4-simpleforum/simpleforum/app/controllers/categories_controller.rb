class CategoriesController < ApplicationController
  before_action :set_category, only: [:edit, :update, :show, :destroy]

  def index
    @categories = Category.all
  end

  def show
    @topics = @category.topics
  end

  def new
    @category = Category.new
  end

  def create
    @category = Category.new(category_params)
    @category.user = current_user

    if @category.save
      redirect_to categories_path, notice: 'Category was successfully created.'
    else
      render 'categories/new', alert: 'Category was not created.'
    end
  end

  def edit
  end

  def update
    if @category.update(category_params)
      redirect_to categories_path, notice: 'Category was successfully updated.'
    else
      render :edit, alert: 'Category was not updated.'
    end
  end

  def destroy
    @category.destroy
    redirect_to categories_path, notice: 'Category was successfully destroyed.'
  end

  private
    def set_category
      @category = Category.find_by_id(params[:id])
    end

    def category_params
      params.require(:category).permit(:name, :description, :priority)
    end
end
