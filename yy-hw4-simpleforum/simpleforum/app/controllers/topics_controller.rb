class TopicsController < ApplicationController
  before_action :set_topic, only: [:show, :destroy, :edit, :update]

  def new
    @topic = Topic.new
  end

  def create
    @topic = Topic.new(topic_params)
    @topic.user = current_user
    @topic.views = 0

    if @topic.save
      @post = @topic.posts.new
      @post.content = topic_params[:content]
      @post.priority = 0
      @post.user = current_user
      @post.save
      redirect_to @topic, notice: 'Topic was successfully created.'
    else
      render :new, alert: 'Topic was not created.'
    end
  end

  def show
    @posts = @topic.posts
    @topic.views += 1
    @topic.save
  end

  def edit
  end

  def update
    if @topic.update(topic_params)
      redirect_to @topic, notice: 'Topic was successfully updated.'
    else
      render :edit, alert: 'Topic was not updated.'
    end
  end

  def destroy
    @topic.destroy
    redirect_to root_path, notice: 'Topic was successfully destroyed.'
  end

  private
    def set_topic
      @topic = Topic.find_by_id(params[:id])
    end

    def topic_params
      params.require(:topic).permit(:name, :content, :priority, :category_id)
    end
end