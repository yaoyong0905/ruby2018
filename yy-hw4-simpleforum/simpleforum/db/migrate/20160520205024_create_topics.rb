class CreateTopics < ActiveRecord::Migration
  def change
    create_table :topics do |t|
      t.string :title
      t.string :description
      t.integer :priority
      t.integer :posts_count
      t.integer :views
      t.timestamps null: false
    end
  end
end
