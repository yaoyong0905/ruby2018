class AddFieldsToUser < ActiveRecord::Migration
  def change
    add_column :users, :activity_count, :integer
    add_column :users, :signature, :string
    add_column :users, :nick, :string
  end
end
