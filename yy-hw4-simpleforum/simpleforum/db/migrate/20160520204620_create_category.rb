class CreateCategory < ActiveRecord::Migration
  def change
    create_table :categories do |t|
      t.string :name
      t.string :description
      t.integer :priority
      t.integer :topics_count
      t.integer :posts_count
      t.timestamps null: false
    end
  end
end
