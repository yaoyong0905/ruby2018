class ChangeUsers < ActiveRecord::Migration
  def change
    remove_column :users, :activity_count
    add_column :users, :posts_count, :integer
    add_column :users, :topics_count, :integer
    add_column :users, :categories_count, :integer
  end
end
