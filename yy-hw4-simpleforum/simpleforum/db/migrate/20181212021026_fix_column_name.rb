class FixColumnName < ActiveRecord::Migration
  def change
    rename_column :topics, :content, :description
    rename_column :topics, :name, :title 
  end
end
