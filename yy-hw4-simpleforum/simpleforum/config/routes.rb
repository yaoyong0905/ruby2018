Rails.application.routes.draw do
  root "categories#index"
  devise_for :users

  resources :categories
  resources :topics, except: [:index] do
    resources :posts, except: [:index, :show]
  end
end
