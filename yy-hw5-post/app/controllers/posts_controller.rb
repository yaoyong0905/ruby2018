class PostsController < ApplicationController
	before_action :set_post, only: [:edit, :update, :show, :destroy]

  def index
    @posts = Post.all
  end

  def new
    @post = Post.new
  end

  def create
    @post = Post.new(post_params)
    if @post.save
      # redirect_to posts_path
      redirect_to(posts_path, :notice => 'Post was successfully created.')
        # format.html  { redirect_to(@post, :notice => 'Post was successfully created.') }
        # format.json  { render :json => @post, :status => :created, :location => @post }
    else
      render :action => 'new'
    end
  end

  def edit
    @post = Post.find (params[:id])
  end

  def update
    @post = Post.find (params[:id])
    if @post.update_attributes (post_params)
      redirect_to posts_path
    else
      render :action => :edit
    end
  end

  def destroy
    @post = Post.find (params[:id])
    @post.destroy
    redirect_to posts_path
  end

  def show
    @post = Post.find (params[:id])
  end
  
   
  private
    def set_post
      @post = Post.find_by_id(params[:id])
    end

    def post_params
      params.require(:post).permit(:title, :content)
    end
 end 
