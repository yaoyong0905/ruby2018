class Offer
  	attr_accessor :price, :condition, :seller  
 
# getter
def price
  @price
end

def condition
  @condition
end

def seller
  @seller
end



# setter
def price=(val)
  @price = val
end


def condition=(val)
  @condition = val
end

 
def seller=(val)
  @seller = val
end


end